﻿/*
 *  PythonInC: A lib that you can use Python - Like code in C/C++
 *  Copyright(C) 2022  FLCoder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.If not, see <https://www.gnu.org/licenses/>.
 */
#include "pystd.h"
#include <cstdio>
#include <cmath>
namespace pystd {
	char* version()
	{
		return "0.0.1";
	}
	char* vers() {
		return "0.0.1";
	}
	void print_license() {
		std::puts(license);
	}
	void print_license(char* a) {
		std::sprintf(a, license);
	}
	int abs(int x)
	{
		return x>=0?x:-x;
	}
	short abs(short x)
	{
		return x >= 0 ? x : -x;
	}
	char abs(char x)
	{
		return x >= 0 ? x : -x;
	}
	long abs(long x)
	{
		return x >= 0 ? x : -x;
	}
	long long abs(long long x)
	{
		return x >= 0 ? x : -x;
	}
	float abs(float x)
	{
		return x >= 0 ? x : -x;
	}
	double abs(double x)
	{
		return x >= 0 ? x : -x;
	}
	long double abs(long double x)
	{
		return x >= 0 ? x : -x;
	}
	unsigned int abs(unsigned int x)
	{
		return x;
	}
	unsigned short abs(unsigned short x)
	{
		return x;
	}
	unsigned char abs(unsigned char x)
	{
		return x;
	}
	unsigned long abs(unsigned long x)
	{
		return x;
	}
	unsigned long long abs(unsigned long long x)
	{
		return x;
	}
	template<typename number>number abs(number x) {
		return x.__abs__();
	}

}