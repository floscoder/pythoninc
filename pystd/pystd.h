﻿/*
 *  PythonInC: A lib that you can use Python - Like code in C/C++
 *  Copyright(C) 2022  FLCoder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PYSTD_LIB_H
#define PYSTD_LIB_H
namespace pystd {
constexpr char license[] = "PythonInC: A lib that you can use Python-Like code in C/C++\nCopyright(C) 2022  FLCoder\n\nThis program is free software : you can redistribute it and /or modify\nit under the terms of the GNU General Public License as published by\nthe Free Software Foundation, either version 3 of the License, or \n(at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, \nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the\nGNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License\nalong with this program.If not, see <https://www.gnu.org/licenses/>.";
	char* version();
	char* vers();
	//version function
	/*
	 * eg.
	 * char *a=pystd::vers();
	 * puts(a);
	 * There will be "0.0.1" on the screen.
	 */
	void print_license();//Print GPL3.0 on the screen.
	void print_license(char*);//Print GPL3.0 to the c_str.
	int abs(int x);
	short abs(short x);
	char abs(char x);
	long abs(long x);
	long long abs(long long x);
	float abs(float x);
	double abs(double x);
	long double abs(long double x);
	unsigned int abs(unsigned int x);
	unsigned short abs(unsigned short x);
	unsigned char abs(unsigned char x);
	unsigned long abs(unsigned long x);
	unsigned long long abs(unsigned long long x);
	template<typename number>number abs(number x);
	//x: A int or a object with function __abs__()
}
#endif // !PYSTD_LIB_H